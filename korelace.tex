
\label[korelace] \chap Určení rychlosti korelační metodou

% Většina převzata z učebního textu SME 08

V matematické statistice vyjadřuje korelace vzájemný vztah dvou (nebo i více) veličin (hodnot, znaků, procesů). Korelační koeficient $ \varrho $ nám říká informaci o tom, jak moc se změnou jedné veličiny změní veličina druhá, respektive jak moc si jsou obě veličiny podobné. Hodnota korelačního koeficientu rovna plus 1 znamená přímou závislosti mezi veličinami (signály mají stejný průběh), hodnota 0 značí nekorelovatelnost daných signálů (není mezi nimi žádná známá závislost). 
Čím bližší hodnota k jedné, tím si jsou signály podobnější.

Korelační koeficient se spočítá podle vzorce 

$$
\varrho (X,Y) = {cov(X,Y) \over \sigma_{X}\sigma_{Y}},\eqmark
$$

kde $ X $ a $ Y $ jsou náhodné veličiny, $ cov(X,Y) $ je kovariance veličin $ X $ a $ Y $ a $ \sigma $ je směrodatná odchylka dané veličiny.
Korelace je kovariance dvou normovaných náhodných veličin.
V případě dvou náhodných signálů tvoří jednotlivé kombinace korelačních koeficientů korelační matici 2x2

$$
M = \pmatrix{
\varrho (X,X) & \varrho (X,Y) \cr
\varrho (Y,X) & \varrho (Y,Y) \cr
} = \pmatrix{
1 & \varrho (X,Y) \cr
\varrho (X,Y) & 1 \cr
}, \eqmark
$$

kde v druhé části využíváme vlastnosti, že $ \varrho (X,X) = \varrho (Y,Y) = 1 $ a $ \varrho (X,Y) = \varrho (Y,X) $.

V teorii signálů se korelací, respektive vzájemnou (cross) korelací, vyjadřuje míra podobnosti průběhů dvou signálů. Využití korelace pro měření rychlosti vychází z jednoduchého principu -- pomocí dvou snímačů vzdálených od sebe o pevnou vzdálenost \textit{d}~ve směru pohybu snímáme nějakou náhodnou proměnnou veličinu. Díky posunutí senzorů ve směru pohybu bude signál z druhého senzoru opožděn oproti prvnímu o~časový interval $ \Delta t $, který je závislý na rychlosti pohybu. Měřenou veličinou může být například difúzní odraz záření od nerovností na povrchu zkoumaného objektu, u hrubšího povrchu jeho přesný profil (přesná vzdálenost od senzoru) či libovolná jiná náhodná vlastnost povrchu (např. magnetické vlastnosti). Schéma principu měření rychlosti korelační metodou je na Obrázku \ref[fig:correlation].

Pro korelační měření nejsou vhodné pravidelně se opakující struktury/vlastnosti povrchu předmětu, protože vedou na mnohoznačnost měření. Tomu lze ale částečně zabránit zajištěním, že se bude korelovat pouze signál z jedné (pro oba senzory té samé) periody opakujícího se signálu, například signálu z přejezdu senzorů nad jedním a tím samým pražcem -- třeba tím, že vzdálenost $ d $ mezi snímači bude menší než minimální vzdálenost mezi dvěma pražci. 

\medskip \clabel[fig:correlation]{Schéma korelačního měření rychlosti}
\picw=8cm \cinspic obr/obr2.png
\caption/f Schéma korelačního měření rychlosti -- dva senzory $ {S_{1}} $ a $ S_{2} $ ve vzájemné vzdálenosti $ d $ od sebe měří libovolnou stejnou náhodnou vlastnost povrchu, nad kterým se pohybují rychlostí $ v $.
\medskip

Pro výpočet korelace si označíme (diskrétní) signál z prvního senzoru $ s_{1}(nT) $ a~z~druhého senzoru $ s_{2}(nT) $. Cílem je nalézt v záznamu z druhého senzoru opožděný záznam prvního senzoru (viz Obrázek \ref[fig:correlation2]). Hledáme takové zpoždění $ \tau $, které minimalizuje střední kvadratickou odchylku $ D $ mezi signály, tedy  

$$
D = {1 \over N}\sum_{n=1}^{N}(s_{1}(nT-\tau) - s_{2}(nT))^{2},\eqmark
$$

kde $ N $ je počet vzorků signálů, na kterých provádíme korelaci. Závorku v sumě můžeme roznásobit, čímž dostaneme následující
$$
D = {1 \over N}\sum_{n=1}^{N}s_{1}^{2}(nT-\tau) - {1 \over N}\sum_{n=1}^{N}2 s_{1}(nT-\tau)s_{2}(nT) + {1 \over N}\sum_{n=1}^{N}s_{2}^{2}(nT).\eqmark
$$

Z rovnice je patrné, že první a poslední člen bude vždy nezáporný, nemusíme je dále uvažovat. Pro minimalizaci \textit{D} potřebujeme maximalizovat prostřední člen, tedy hledáme maximum funkce
$$
R(\tau) = {1 \over N}\sum_{n=1}^{N}s_{1}(nT-\tau)s_{2}(nT)\eqmark
$$
pro všechna možná $ \tau $.

Pro spojité signály je hledání korelace dvou signálů analogické, hledáme maximum funkce
$$
R(\tau) = \lim_{T\to\infty} {1 \over T} \int_{0}^{T}s_{1}(t-\tau)s_{2}(t)dt.\eqmark
$$

Oba tyto předpisy nápadně připomínají definici konvoluce, pomocí které se také v~praxi jednotlivé hodnoty $ R(\tau) $ počítají. Nás ale v obou případech zajímá hodnota $ \tau $, ve které $ R(\tau) $ nabývá maxima, tedy hledáme
$$
\tau_{\max} =\arg\max_\tau R(\tau).\eqmark
$$

V diskrétních signálech není výstupem korelace přímo časová konstanta $ \tau_{\max} $, ale počet vzorků $ n $, o kolik jsou jednotlivé signály mezi sebou posunuté. Při pevně dané snímací frekvenci $ f $ je převod následující

$$
\tau_{\max} ={n \over f}.\eqmark
$$

Ve chvíli, kdy známe $ \tau_{\max} $ a vzdálenost $ d $ mezi senzory, je určení rychlosti již triviální
$$
v = {d \over \tau_{\max}}.\eqmark
$$

\medskip \clabel[fig:correlation2]{Princip korelačního měření rychlosti}
\picw=15cm \cinspic obr/obr18.png
\caption/f Princip korelačního měření rychlosti s vyznačenou hledanou hodnotou $ \tau_{\max} $ 
\medskip

V praxi nikdy nebudou výstupní signály z obou senzorů identické, budou nestejně zatíženy šumem a jinými rušivými vlivy. Při porovnávání signálů mezi sebou se avšak vliv tohoto šumu dokáže díky průměrování dobře potlačit (čím delší signály korelujeme, tím lépe) -- střední hodnota šumu je nulová. To je právě jedna z velkých výhod korelace -- velká odolnost vůči rušení. \cite[Gogoasa1996, Qingmin2012]