
\label[simulace] \chap Simulace korelačního měření s~vybranými senzory

% \sec Simulace cross-korelačního měření

Pomocí programu {\em Matlab} byl vytvořen simulátor jízdy soupravy se senzory nad kolejovým svrškem. Skládá se ze dvou částí -- vytvoření modelu profilu kolejového svršku a~samotné simulace jízdy a měření senzorů nad ním. Následně byly do simulátoru dosazeny parametry vybraných skutečných senzorů.

\sec Model kolejového svršku

Nejprve se vytvoří 1D model kolejového svršku, který zachycuje profil trati -- prostor (uprostřed) mezi kolejemi ve směru jízdy. V tomto prostoru se obvykle nachází pouze periodicky se opakující pražce a železniční štěrk mezi nimi (viz Obrázek \ref[logo1]). Pražce mají většinou hladkou rovnou strukturu, ale štěrk mezi pražci je rozsypán náhodně -- má náhodnou strukturu, což je právě pro použití korelace důležité.

Vstupem do modelu kolejového svršku jsou následující parametry:

\begitems
* Délka generovaného modelu:  
definuje délku trati v centimetrech, pro kterou se generuje tento model. Výchozí hodnota je 10 000 cm, tedy 100 metrů.

* Velikost pražce: 
definuje velikost (šířku) pražce v centimetrech. Výchozí hodnota je 25 cm (obvykle se pohybuje mezi 15--30 cm \cite[Plasek2006-2]).

* Vzdálenost dvou pražců: 
definuje vzdálenost v centimetrech, jak daleko jsou od sebe středy dvou pražců. Výchozí hodnota je 60 cm (nejčastější hodnota; obvykle se pohybuje mezi 55--70 cm \cite[EfBiXvUkbQpoZBSQ]).
\enditems

Po zadání těchto parametrů se vytvoří pole o velikosti požadované délky generovaného modelu. Do pole jsou následně vloženy náhodné hodnoty dle normálního (Gaussova) rozdělení s jednotkovým rozptylem. To představuje náhodně rozsypané kameny štěrku mezi kolejemi.

Následně jsou do pole vloženy s hodnotou 0 pražce podle zadaných hodnot velikosti pražce a vzdálenosti dvou pražců (mezera mezi pražci je dána vzdáleností dvou pražců minus velikost pražce). Pražce jsou tedy posunuty do základní (nulové, střední) výšky. 

Výsledné pole hodnot je ještě vyhlazeno (aby se eliminovaly velmi prudké skoky tam a zpět) a předáno jako parametr do další části programu -- do části simulující jízdu se senzory. Ukázka části vygenerovaného profilu kolejového svršku je na Obrázku \ref[fig:profil].

\midinsert \clabel[fig:profil]{Vygenerovaný profil kolejového svršku}
\picw=15cm \cinspic obr/svrsek3-a.png
\caption/f Ukázka vygenerovaného profilu kolejového svršku (modře). Hnědým obdélníkem jsou znázorněny pražce (zde o šířce 15 cm). Prostor pod modrou čárou představuje kromě pražců jednotlivé kameny štěrku.
\endinsert

\sec Simulace jízdy se senzory

V druhé části programu se simuluje samotná jízda se senzory nad dříve vygenerovaným profilem kolejového svršku. Vždy se provádí simulace jízdy se dvěma senzory se stejnými vlastnostmi (parametry), které jsou pro reálné snímače vyčteny z katalogového listu. Senzory jsou od sebe navzájem posunuty o vzdálenost $ d $ ve směru pohybu. Na základě simulovaných výstupů ze senzorů se následně provádí výpočet korelace a porovnává se reálná rychlost pohybu s vypočítanou rychlostí pomocí korelace.

Parametry této části simulace jsou následující:

\begitems
* Model kolejového svršku, který byl vygenerován podle předchozí podkapitoly.

* Rychlost pohybu: 
definuje rychlost pohybu $ v $ v km/h, jakou se simulovaně pohybují senzory nad kolejovým svrškem (tedy jak rychle vlak jede).

* Snímací frekvence senzorů: 
definuje frekvenci v Hz, s jakou snímají senzory profil kolejového svršku (kolik měření provedou za sekundu).

* Vzdálenost senzorů: 
definuje vzdálenost $ d $ v centimetrech, jak daleko od sebe jsou za sebou ve směru pohybu oba snímače. Výchozí hodnota je 200 cm (2 metry). Rozvor kol (vzdálenost mezi dvěma dvojkolími) podvozku vlaku je obvykle mezi 180 a 250 cm \cite[k9AAad3FRv3CDCG8], hodnota je tedy volena tak, aby bylo možné senzory umístit na jeden společný podvozek.

* Perioda aktualizace: 
definuje dobu, po kolika sekundách jízdy se provádí výpočet korelace. Výchozí hodnota je 1 sekunda. Výpočet korelace se v tomto případě neprovádí nad celým 100 metrovým úsekem trati, ale pouze nad tak dlouhým úsekem, který vlak projede danou rychlostí za 1 sekundu.

* Výška senzoru nad povrchem:
definuje výšku v centimetrech, v jaké se pohybují senzory nad profilem kolejového lože. Výchozí hodnota je 40 cm.

* Zorný úhel senzoru (FoV) / rozměr bodu měření:
definuje zorný úhel ve stupních (případně přímo rozměr bodu měření), ve kterém snímá senzor daný povrch. V ideálním případě by byly všechny senzory bodové, to však v praxi není možné. 

* Počet desetinných míst:
definuje rozlišení senzoru (nejmenší hodnotu, kterou je senzor schopný rozlišit). Udává počet desetinných míst, na který se zaokrouhluje měřená hodnota. Nula odpovídá rozlišení 1 cm, jednička odpovídá rozlišení 0,1 cm atd.

* Směrodatná odchylka:
definuje velikost směrodatné odchylky přenosnosti měření senzoru v centimetrech.

\enditems

Po spuštění se nejprve převede rychlost na m/s a pro zadanou hodnotu rychlosti a snímací frekvence se spočítá vzdálenost dvou vzorků od sebe. Podle zadaného zorného úhlu a výšky senzoru nad povrchem se také vypočítá velikost snímané oblasti (stopy), kterou senzor v jeden moment zaznamenává a jejíž vzdálenost nějakým způsobem \uv{průměruje} (nebo dostaneme zadanou rovnou tuto velikost). K tomu se vytvoří normované koeficienty okénkové funkce o dané velikosti (použito je zde Tukey window\urlnote{https://www.mathworks.com/help/signal/ref/tukeywin.html} s parametrem 0,5).

Dále se vytvoří 2 pole (pro každý senzor jedno) o velikosti součinu snímací frekvence a periody aktualizace -- do těchto polí se budou následně ukládat \uv{naměřená} data a~bude se s nimi provádět korelace.

Nyní je pro každou hodnotu v tomto poli nasimulována měřená hodnota. V daném jednom bodě měření se vezme oblast kolem tohoto bodu o velikosti snímané oblasti senzoru a konvolucí se na ni aplikují předpočítané koeficienty okénkové funkce, čímž dostaneme jednu číselnou hodnotu. Následně se tato hodnota zatíží bílým šumem se zadanou směrodatnou odchylkou a~výsledek se po zaokrouhlení na daný počet desetinných míst uloží do výsledného pole. Poté se výpočet posune dále o vzdálenost mezi dvěma vzorky, kde se celý proces opakuje. 

Výpočet hodnot pro druhý senzor probíhá stejným způsobem, pouze jsou dané body, ve kterých probíhá výpočet, posunuty o vzdálenost mezi senzory (uvažujeme tedy, že snímače měří synchronizovaně, oba ve stejnou chvíli).

Po vypočítání všech hodnot v polích je provedena s těmito dvěma poli korelace. Po nalezení maximální hodnoty $ R(\tau) $ a tím určení počtu vzorků, o které jsou mezi sebou oba signály posunuté, se při znalosti zadané vzdálenosti senzorů a jejich snímací frekvence určí rychlost, s~jakou se senzory pohybují. Ta je následně porovnána se zadanou rychlostí na začátku simulace.

\label[simulace-senzory] \sec Simulované senzory

Následně bylo na stránkách různých výrobců dohledáno pět konkrétních optických senzorů, které byly vybrány jako vhodné pro korelační měření. Použity byly 3 Time of flight a 2 triangulační optické snímače s různými vlastnostmi. Jejich katalogové údaje byly následně doplněny do simulace. Přehled vlastností použitých senzorů je uveden v~Tabulce \ref[tab:simulace-senzory]. 

\medskip \clabel[tab:simulace-senzory]{Přehled vlastností simulovaných senzorů}
\ctable{lllllll}{
\low{Senzor} & Snímací & Rozsah & \low{FoV} & \low{Rozlišení} & Směrodatná & \low{Cena} \cr
 & frekvence & měření & & & odchylka & \crl
TF-Luna & až 250 Hz & 0,2--8 m & 2 $^\circ$ & 10 mm & 0,25 cm & 500 Kč \cr
TF Mini-S & až 1 kHz & 0,1--12 m & 2 $^\circ$ & 10 mm & 0,5 cm & 1k Kč \cr
TF 03-100 & až 10 kHz & 0,1--100 m & 0,5 $^\circ$ & 10 mm & 3 cm & 5k Kč \cr
OM70-P0600 & 2 kHz & 0,1--0,6 m & 0,3 mm & 3 \micro m & 9 \micro m & 43k Kč \cr
ILD1420-500 & až 4 kHz & 0,1--0,6 m & 1,1 mm & 0,5 \micro m & 40 \micro m & 60k Kč\cr
}
\caption/t Přehled vlastností simulovaných senzorů \cite[p5o1nSbukRg12gMK, lRwq87DQVhyAXBT2, yIL1qWe5wYHOadly]. Sloupec FoV odpovídá buď zornému úhlu jako takovému nebo průměru měřené oblasti (stopy).
\medskip

První 3 senzory jsou od čínského výrobce Benewake a pracují na principu Time of flight, zbylé dva -- snímač OM70-P0600 od švýcarské firmy Baumer a senzor ILD1420-500 od firmy Micro-Epsilon z Německa -- pracují na triangulačním principu. Ceny v~tabulce jsou přibližné a odpovídají ceně za jeden kus, pro použití korelace s dvěma senzory je nutné tedy tuto cenu vynásobit dvěma.

\sec Výsledky

Nejprve byly simulováním ověřeny vztahy mezi přesností měření a některými zadanými parametry -- vždy se pro dané měření měnil pouze jeden parametr, ostatní zůstaly stejné:

\begitems
* Snímací frekvence:  
čím větší snímací frekvence, tím přesnější dostáváme výsledky rychlosti z korelace. Zvýšením snímací frekvence na dvojnásobek se odchylka sníží na polovinu.

* Vzdálenost mezi senzory: 
opět platí, že čím větší vzdálenost, tím přesnější dostáváme výsledky rychlosti. Zvýšením vzdálenosti mezi senzory na dvojnásobek se odchylka opět sníží na polovinu. Nicméně čím větší vzdálenost, tím více je nutné posunout minimální rychlost, od které lze korelaci provést. Navíc možnosti umístění senzoru na vozidlo jsou omezené, oba senzory musí i v oblouku procházet tím samým místem, takže není možné je umístit kamkoliv. Umístění na společný podvozek je tedy nejlepší variantou.

* Vzdálenost dvou pražců, velikost pražce a výška senzoru nad povrchem: 
jejich přesné hodnoty parametrů (v rámci reálných mezí) nehrají roli.

* Perioda aktualizace:
zvyšování její hodnoty nad 1 sekundu nemá žádný velký přínos. Snižováním její hodnoty ale opět dojde k tomu, že při nižších rychlostech za tuto dobu souprava neujede ani vzdálenost mezi oběma senzory. Např. při vzdálenosti senzorů 200 cm a periodě aktualizace 0,5 s začne korelace vracet smysluplné výsledky až při rychlostech přes 20 km/h.
\enditems

Cílem je tedy použít senzory s co největší snímací frekvencí a umístit je co nejdále od sebe, což nám zajistí velmi dobré výsledky při středních a vyšších rychlostech.

Dále byla s parametry senzorů uvedených v Tabulce \ref[tab:simulace-senzory] provedena samotná simulace. Za rychlost pohybu byly zvoleny hodnoty od 8 km/h po 2 km/h do 160 km/h (menší rychlosti než 8 km/h nemá smysl uvažovat -- korelace se v tomto zapojení pro velmi pomalou jízdu nehodí). Ostatní parametry byly ponechány na výchozích hodnotách. Výstup ze simulace je na Obrázku \ref[fig:simulace-vysledky].

\midinsert \clabel[fig:simulace-vysledky]{Odchylka od skutečné rychlosti pro vybrané senzory}
\picw=20cm \cinspic obr/simulace-h40-period2-small.png
\caption/f Odchylka vypočítané rychlosti pomocí korelace od skutečné rychlosti pro pětici vybraných optických senzorů. Vzdálenost senzorů = 200 cm, perioda aktualizace = 1 s, výška senzoru nad povrchem = 40 cm.
\endinsert

Z obrázku vyplývá, že dle simulace je použití senzorů pro korelační měření možné a~poskytují poměrně přesné informace o aktuální rychlosti. (Při důvěře v přesnost hodnot udávaných katalogových údajů, na které se bohužel ne vždy lze spolehnout.) Korelace je velmi silný nástroj, dokáže dát přesné výsledky i s nepřesnými senzory. 

Závislost odchylky na snímací frekvenci senzorů je patrná i zde. Senzor TF-Luna je v grafu ukončen už při rychlosti 106 km/h, při vyšších rychlostech poskytuje odchylky v řádech desítek i stovek km/h. Snímací frekvence tohoto snímače je pro vyšší rychlosti naprosto nedostatečná.

Senzor TF 03-100 poskytuje sice nejlepší výsledky (díky velmi vysoké snímací frekvenci), má ale velkou směrodatnou odchylku, proto jsou jeho výsledky velmi podobné se senzorem ILD1420-500, který má snímací frekvenci dva a půl krát menší. Nicméně ve prospěch TF 03-100 je i to, je výrazně levnější.
