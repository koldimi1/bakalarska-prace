
% \chap Senzory pro korelační měření rychlosti vlaku
\label[senzory] \chap Uvažované senzory

Pro korelační měření rychlosti vlakové soupravy je možno využít více druhů senzorů pracujících na různých fyzikálních principech. Potřebujeme měřit nějakou náhodnou vlastnost měnícího se okolí při jízdě vlaku, případně vlastnost některé z pohyblivých částí vlaku, která přímo závisí na rychlosti. 

V první variantě můžeme využít senzory vzdálenosti, které umístíme na podvozek soupravy do konstantní výšky a budeme měřit přesnou vzdálenost senzoru od kolejového svršku (oblast mezi kolejemi, Obrázek \ref[logo1]). Tento povrch se kromě pražců obvykle skládá z náhodně vysypaného štěrku o velikosti kamenů 31,5 mm až 63 mm \cite[RGD8ZKJJSM03wk5T], jeho povrch má tedy náhodnou strukturu. Další možností je například detekování pražců, respektive uchycení kolejnic k pražcům. Toto uchycení se skládá obvykle z podkladnice, železniční svěrky a~šroubů, které mají jiné magnetické vlastnosti než samotná kolej a~jsou vystouplé nad okolní štěrk. Snímání uchycení pražce má i jiné výhody, je možné jej detekovat i~v~místech s pevnou jízdní dráhou či na mostech, kde obvykle měření profilu kolejového svršku není možné (kvůli hladkému povrchu mezi kolejemi).

Ve druhé variantě můžeme senzorem pozorovat například průchod značky, kterou umístíme na vhodné místo na kole soupravy (to vyžaduje ale kromě umístění senzorů i zásah do vlaku jako takového a to nechceme). Jinou možností je snímat magnetické vlastnosti kola při otáčení s tím, že předpokládáme, že kolo není homogenní. Práce s~koly vlaku má avšak své nevýhody -- při jízdě poměrně často dochází k~prokluzu kol lokomotivy (díky nízkému součiniteli tření mezi kolejnicí a kolem), navíc kolo nemá v~celé délce konstantní poloměr, směrem ven se zužuje (má kuželovitý či jiný tvar, viz Obrázek \ref[logo2] -- záleží na konkrétním výrobci soupravy a druhu vlaku).

V obou případech ale musíme dávat pozor na dostatečný rozsah měření. Pro využití v~nákladní dopravě potřebujeme pokrýt rychlosti od 0 do 120 km/h, tedy přibližně 0~až 33~m/s. V osobní dopravě je v České republice nejvyšší povolená rychlost na některých úsecích v současné chvíli až 160 km/h, tedy 44 m/s (i když málokterý osobní vlak této rychlosti skutečně při jízdě může dosáhnout). 

Na zrychlování či zpomalování vlaku během jednotlivých měření nemusíme brát příliš ohled, vlaky díky své obrovské hybnosti a setrvačnosti mění svou rychlost velmi neochotně, velmi pomalu. Hodnoty zrychlení vlaku při běžné jízdě se pohybují do 0,5~m/s$ ^{2} $ \cite[c4L34hDcVBt1V2vn]. Nedopustíme se tedy velké chyby, pokud budeme uvažovat rychlost během maximálně sekundového intervalu za konstantní.

\midinsert
\line{\hsize=.5\hsize \vtop{%
      \clabel[logo1]{Kolejový svršek}
      \picw=6cm \cinspic obr/obr4.jpg
      \caption/f Kolejový svršek \cite[Budai1052016]
   \par}\vtop{%
      \clabel[logo2]{Schéma dvoukolí na kolejích}
      \picw=6cm \cinspic obr/obr3.png
      \caption/f Schéma dvoukolí na kolejích \cite[Sautya16122018]
   \par}}
\endinsert

Pro takto velký rozsah měření rychlostí potřebujeme pro použití korelace minimálně několik stovek vzorků dat na každý metr, který vlak urazí. To dává zvětšené nároky na snímací frekvenci použitých senzorů, která musí dosahovat hodnot kHz a více. I~v~případě použití snímače se spojitým výstupem by bylo nutné signál pro další zpracování zdigitalizovat pomocí rychlého AD převodníku, což klade nároky na použití dalšího hardwaru.

Dále je popsán princip funkce senzorů vzdálenosti, magnetických senzorů a inerciálních senzorů vhodných pro korelační měření rychlosti vlaku.

V závěru kapitoly jsou popsány také mikrovlnné senzory pracující na jiném principu -- využívají pro měření rychlosti místo korelace Dopplerův jev, čili změnu frekvence vlnění vlivem vzájemného pohybu přijímače a vysílače.


\sec Senzory vzdálenosti

Senzorů vzdálenosti pracujících na nejrůznějších fyzikálních principech je celá řada. Pro naše účely se nejvíce hodí optické snímače vzdálenosti či ultrazvukové senzory, které jsou zde popsány dále. Uvažujeme snímače umístěné na podvozku lokomotivy, kde je vzdálenost od povrchu železničního svršku zhruba 30 až 50 cm. Vyžadované rozlišení senzoru je v řádu nízkých jednotek milimetrů a lepší, oproti tomu přesnost měření absolutní vzdálenosti zde není důležitá (díky následnému normování signálu). 

Kromě tohoto je důležitým údajem velikost zorného pole (Field of view -- FoV), který charakterizuje jak velkou plochu snímaného povrchu senzor při svém měření zabírá (nikdy se v praxi nebude jednat o bodové senzory). Pro rozlišení co nejmenších detailů je třeba mít zorné pole co nejmenší, jinak bude u hrubého povrchu železničního svršku docházek k jeho vyhlazování (senzor vzdálenost povrchu v zorném úhlu nějakým způsobem \uv{průměruje}\fnote{Nebude se jednat přesně průměr, ale o nějakou váženou 2D okénkovou funkci (window function).}). Těmito parametry spolu s~požadavkem na vysokou snímací frekvenci jsou dány požadavky na použitelné senzory.
% požadavky (opakující se slovo)

\secc Optické senzory

Optických senzorů je mnoho, dosahují vysokých přesností (\micro m, nm) a velkých rozsahů měření (\micro m až mnoho kilometrů). Některé druhy optických snímačů jsou silně závislé na odrazivosti měřeného předmětu (difúzní) či přímo vyžadují odrazku na měřeném objektu (reflexní senzory). Zmíněné senzory jsou kvůli této závislosti pro naše použití nevhodné, proto je dále neuvažujeme. Malou či žádnou závislost na odrazivosti zkoumaného tělesa mají triangulační a Time of flight senzory. Obecnou nevýhodou optických snímačů je jejich malá odolnost vůči špíně a prachu, při zašpinění optických prvků (výstupní/vstupní čočky) mohou přestat úplně fungovat.

\begitems

* {\sbf Triangulační senzory}

Triangulační senzory využívají, jak napovídá název, princip optické triangulace. Obsahují zdroj záření -- laser nebo LED -- kterým svítí na zkoumaný objekt, na kterém vytváří světelný bod. Záření se od objektu odrazí zpět, projde optickou soustavou a~dopadne na polohově citlivý detektor (PSD -- position sensitive photo-detector) či na CCD (charge-coupled device) snímač. Podle místa dopadu odraženého paprsku na detektor se určí úhel dopadu paprsku. Ze znalosti úhlu dopadu a vzdálenosti snímače od zdroje záření se jednoduchou triangulací vypočítá vzdálenost světelného bodu od senzoru. \cite[Vojacek1372015] 

Tyto senzory jsou málo závislé na odrazivosti a tvaru povrchu, protože stačí jeden jediný odražený paprsek (s intenzitou větší, než je detekovatelné minimum) ve směru snímače pro určení vzdálenosti \cite[Vojacek1372015].
Rozsah měření těchto senzorů se pohybuje od půl milimetru do jednoho metru, dosahují měřící frekvence stovek Hz až desítky kHz a jsou velmi přesné. Schéma fungování triangulačního snímače je na Obrázku~\ref[fig:triangulace].

% citace ze skript, citace rozsahu uvedenych parametru

\midinsert \clabel[fig:triangulace]{Princip funkce triangulačního senzoru}
\picw=8cm \cinspic obr/obr5.png
\caption/f Princip funkce triangulačního senzoru vzdálenosti \cite[n5riGvWOnAkn3vQP]
\endinsert


* {\sbf Time of flight senzory}

Senzory měřící dobu letu paprsku, neboli \uv{Time of flight} senzory, měří vzdálenost přímou metodou. Rychlost šíření světelného paprsku je jedna ze základních fyzikálních konstant, která je pevně daná. Světlo urazí za 1 ns vzdálenost 30 cm -- pro přesná měření na krátké vzdálenosti jsou kladeny velké nároky na přesnost měření času. Senzor vyšle velmi krátký světelný pulz a změří dobu, za kterou se jeho odraz od předmětu vrátí zpět. Z toho se už jednoduše určí hledaná vzdálenost předmětu. Rozsah měření těchto senzorů začíná na desetině metru (právě kvůli velkým nárokům na přesné měření času) a dosahuje až několika kilometrů. Rozlišení těchto snímačů je menší v porovnání s triangulačními senzory a dosahují měřící frekvence v jednotkách kHz. Schéma fungování tohoto senzoru včetně rovnice pro výpočet vzdálenosti je na Obrázku \ref[fig:ToF].

\midinsert \clabel[fig:ToF]{Princip funkce Time of flight senzoru}
\picw=9cm \cinspic obr/obr6.jpg
\caption/f Princip funkce Time of flight senzoru včetně vzorce pro výpočet vzdálenosti~\cite[QTY47UQx9Yxzof5i]
\endinsert

\enditems

\secc Ultrazvukové senzory

Princip funkce ultrazvukových senzorů je obdobný jako u Time of flight senzorů, jen se místo světelného pulzu používá ultrazvuková vlna. Ultrazvuk je akustické vlnění s~vyššími frekvencemi, než jsou frekvence slyšitelné lidským uchem, tedy více než 20~kHz.

Základem senzoru je piezoelektrický měnič, který svým rozkmitáním vyšle ultrazvukovou vlnu. Ta se šíří prostředím rychlostí zvuku a odrazí se od snímaného předmětu zpět. Následně je v (obvykle tom samém) měniči detekována.

Podle doby letu a rychlosti šíření vlny se určí vzdálenost daného předmětu. Rychlost šíření zvukové vlny je však oproti světlu velmi malá, nelze tedy dosáhnout velmi vysokých snímacích frekvencí. Navíc v tomto případě není ani tato rychlost v čase konstantní, záleží na parametrech okolního prostředí, především na tlaku a teplotě. Pro korelační měření ale není konkrétní rychlost šíření důležitá, hlavní je, že oba použité senzory jsou ovlivněny okolními podmínkami stejně. Při umístění snímače do blízkosti kola vlaku může docházet k ovlivňování senzorů od vysokofrekvenčního akustického rušení od vlaku, především při brzdění. Proto by bylo vhodné použít senzory pracující na co nejvyšší frekvenci ultrazvukového spektra, stovky kHz a více.

Další nevýhodou je tzv. mrtvý čas, který je způsobený dokmitáváním měniče po odeslání vlny. V této době není senzor schopný detekovat navrácenou vlnu, snímač je během této doby slepý. Tuto nevýhodu lze částečně eliminovat použitím druhého měniče, kdy jeden slouží jako vysílač (sender, transmitter) a druhý jako přijímač (receiver) signálu. Naopak velkou výhodou oproti optickým senzorům je odolnost vůči prachu a~špíně. Snímače jsou tvořeny kmitající membránou, která svými mechanickými kmity brání usazování nečistot a dochází tak k samočištění povrchu senzoru. 

Rozsah měření těchto senzorů je od jednotek milimetrů do desítek metrů. Schéma funkce ultrazvukového snímače je na Obrázku \ref[fig:ultrazvuk]. \cite[Vojacek772017]

\midinsert \clabel[fig:ultrazvuk]{Princip funkce ultrazvukového senzoru}
\picw=10cm \cinspic obr/obr7.jpg
\caption/f Princip funkce ultrazvukového senzoru \cite[2rztyHLc6qzKkJb5]
\endinsert

\secc Srovnání senzorů vzdálenosti

Porovnání jednotlivých základních parametrů průmyslově vyráběných senzorů vzdálenosti je v Tabulce \ref[tab:senzory].

\medskip \clabel[tab:senzory]{Porovnání základních parametrů senzorů vzdálenosti}
\ctable{lllll}{
\hfil Druh senzoru & Rozsah měření & Rozlišení & Měřící frekvence & Cena \crl \tskip4pt
Triangulační & 0,5--1 000 mm & 30--10 000 nm & 1--50 kHz & 10k Kč\cr
Time of flight & 0,1--3 000 m & 0,1--10 mm & 0,1--10 kHz & 1k Kč\cr
Ultrazvukový & 2--6 000 mm & 0,3--5 mm & max. stovky Hz & 2k Kč\cr
}
\caption/t Porovnání základních parametrů senzorů vzdálenosti. Cena je orientační pro senzory měřící na min. vzdálenost 50 cm s min. frekvencí 1 kHz (s výjimkou ultrazvukových).
\medskip

Ze srovnání je patrné, že pro naše uvažované korelační měření rychlosti se nejlépe hodí optické triangulační senzory. Mají vhodný rozsah měření, velmi velké rozlišení a~dosahují velkých snímacích frekvencí, jsou avšak z dané skupiny výrazně dražší. Dobrých vlastností dosahují také Time of flight senzory, které mají vhodný rozsah měření, dosahují velkých snímacích frekvencí, ale mají nižší rozlišení. Poslední v pořadí skončily ultrazvukové senzory, které jsou limitovány především malými snímacími frekvencemi, což je dáno malou rychlostí šíření zvuku ve vzduchu.

\sec Magnetické senzory

Pro korelační měření rychlosti vlaku je možné také využít magnetické senzory, které reagují na přítomnost magnetického materiálu ve svém okolí. 

První možností je využít senzory magnetické indukce, které reagují změnou indukčnosti cívky na přítomnost magnetického materiálu v okolí. Tyto senzory mají ale obvykle krátký dosah měření v jednotkách centimetrů, pro větší vzdálenosti je nutné použít velké cívky a dosáhnout velkého vyzařovaného výkonu, což je nákladné a energeticky náročné.

Druhou možností magnetických senzorů jsou (diferenciální) senzory vířivých proudů (eddy-current sensor) \cite[Engelberg2001]. Tyto snímače obsahují budící cívku, která je připojena ke zdroji střídavého proudu o vysoké frekvenci. Proud procházející touto cívkou generuje primární střídavé magnetické pole. Ve vodivých materiálech v blízkosti cívky se indukují vířivé proudy (též Foucaultovy proudy – pojmenované po svém objeviteli). 

Lenzův zákon nám říká, že \uv{Indukovaný elektrický proud v uzavřeném obvodu má takový směr, že svým magnetickým polem působí proti změně magnetického indukčního toku, která je jeho příčinou} \cite[9NnaGQESllFxedXA]. Vířivé proudy v materiálu v souladu s tímto zákonem generují magnetické pole opačného znaménka, než je primární pole budící cívky (snaží se bránit změně současného stavu). Výsledné magnetické pole je snímáno dvěma cívkami, které jsou umístěné vedle sebe ve směru pohybu. Uspořádání cívek a schéma zapojení senzoru vířivých proudů je na Obrázku \ref[fig:eddy-current]. 


\midinsert \clabel[fig:eddy-current]{Diferenciální senzor vířivých proudů}
\picw=10cm \cinspic obr/obr8.png
\caption/f Diferenciální senzor vířivých proudů: uspořádání cívek (nahoře), schéma zapojení (dole). Převzato z \cite[Engelberg2001], upraveno.
\endinsert
% Pevyato a upraveno z file:///C:/Users/kolda/Downloads/papers/1-s2.0-S0263224100000439-main.pdf

Snímací cívky jsou zapojené navzájem diferenciálně tak, aby při působení shodného pole na obě cívky bylo výstupní napětí nulové. Snímač vířivých proudů pak reaguje pouze na nerovnoměrnosti podél trati, homogenní pole (kolejnici, šum) nedetekuje, stejnosměrná složka signálu stejně nemá pro korelaci žádný význam. Díky tomu nereaguje ani na další rušivé vlivy, jako je například kmitání odpružení podvozku vlaku při jízdě, které se projevuje změnou vzdálenosti senzoru od země – působí totiž na obě snímací cívky shodně. 

Senzor se hodí pro detekci přítomnosti svěrky a podkladnice, kterými je upevněna kolejnice k pražci a které jsou obě obvykle z elektricky vodivých materiálů. Kromě toho detekuje další vodivé předměty v cestě, například vedlejší koleje u výhybek, případně nejrůznější kabeláž v kolejišti. Bylo by možné s ním detekovat i nehomogenity v kolejnici jako takové, kvůli bezpečnostním předpisům ale není možné umístit cokoliv na vlak těsně nad kolejnici. Ve větší vzdálenosti nad kolejnicí jsou dominantní v detekci právě upevnění kolejnice a další větší vodivé předměty.

Při průjezdu nad upevněním pražce se nejprve zvýší magnetická indukce v jedné snímací cívce, díky čemuž bude mít indukované napětí v obou cívkách různou velikost a po odečtení dostaneme nenulovou hodnotu výstupního napětí. Ve chvíli, kdy je pražec uprostřed senzoru, jsou indukovaná napětí v obou cívkách shodné a výstupní napětí je tak nulové. Nakonec se zvýší magnetická indukce v druhé snímací cívce oproti první a výstupní napětí bude opět nenulové a identické jako při průjezdu nad první cívkou, jen s opačným znaménkem (což je dáno právě diferenciálním zapojením cívek). Průjezd snímače nad pražcem je znázorněn na Obrázku \ref[fig:eddy-current-signal].

\midinsert \clabel[fig:eddy-current-signal]{Výstupní signál diferenciálního senzoru vířivých proudů}
\picw=10cm \cinspic obr/obr9.png
\caption/f Výstupní signál diferenciálního senzoru vířivých proudů při průjezdu nad pražcem se svěrkou. Převzato z \cite[Engelberg2001], upraveno.
\endinsert
% Pevyato a upraveno z file:///C:/Users/kolda/Downloads/papers/1-s2.0-S0263224100000439-main.pdf

Pro použití korelace je nutné použít dva tyto senzory za sebou ve směru pohybu, přičemž jejich vzdálenost musí být menší, než je vzdálenost dvou pražců na trati od sebe. Každý snímač by měl také pracovat na rozdílné budící frekvenci, aby se zabránilo vzájemnému ovlivňování senzorů. Pozor musíme dát také na to, aby výsledné magnetické pole senzoru nenarušilo činnost jiných zařízení ve vozidle nebo v okolí trati. 

I tento senzor dosahuje větších rozměrů a vyšší spotřeby při provozu. Výhodou těchto senzorů ale je, že fungují nezávisle na počasí -- déšť, vítr, prach a podobně na jeho funkci nemají vliv. Dokonce bude funkční i~v~případě, kdy je kolejiště zasypáno sněhem a pražce jsou skryté pod vrstvou sněhu.

Pokud bychom předem znali vzdálenost mezi jednotlivými pražci na trati (tyto vzdálenosti jsou většinou standardizovány), tak by nám pro určení rychlosti vlaku stačilo použít pouze jeden tento snímač. Pomocí Fourierovy řady bychom rozložili výstupní signál senzoru a podle frekvence výskytu jednotlivých pražců a jejich známé vzdálenosti je určení aktuální rychlosti již triviální. \cite[Geistler2004]

Tyto senzory dokáží detekovat velmi přesně i výhybku na trati a odlišit ji od pražce, sekundární magnetické pole vedlejší kolejnice je mnohem silnější než v případě detekování uchycení pražců. Pokud tuto vlastnost spojíme se znalostí železniční sítě (mapy – například z veřejně přístupné Open Street Map\urlnote{https://www.openstreetmap.org/}), kde jsou zachyceny polohy všech kolejí, výhybek a podobně, tak je možné touto kombinací zvýšit přesnost určení polohy a dokonce přesně určit kolej, po které se aktuálně vlak pohybuje. \cite[Hensel2011]

\label[lindometer] \secc Lindometer

Konkrétní realizace diferenciálního senzoru vířivých proudů pro kolejová vozidla je detailně popsána v \cite[Chandran20191201]. V této práci švédský tým sestrojil diferenciální snímač vířivých proudů, kterému dal název \uv{Lindometer}. Měřící systém je koncipován tak, že se pohybuje 65 mm nad temenem hlavy kolejnice a detekuje přítomnost svěrky. Jeho primárním cílem je pomoci s údržbou železničních tratí -- má za úkol detekovat místa (pražce), kde by měla být svěrka držící kolejnici na pražci, ale z nějakého důvodu zde chybí. Na tato místa pak lze cíleně a efektivně posílat pracovníky dráhy na údržbu. Sekundárním cílem bylo testování cross-korelačního měření rychlosti soupravy.

Lindometer obsahuje dva nezávislé 20 cm za sebou umístěné senzory, každý z nich odpovídá schématu z Obrázku \ref[fig:eddy-current]. Primární (budící) cívka jednoho senzoru má rozměry 70 x 155 x 18 mm, uvnitř které jsou umístěny dvě snímací cívky. Aby se zamezilo vzájemnému ovlivňování obou senzorů, je jedna primární cívka buzena střídavým proudem o frekvenci 18 kHz, druhá o frekvenci 27 kHz. V obou případech protéká budící cívkou proud 3 A. Napájení Lindometeru je tedy energeticky poměrně náročné, vyzářený výkon je velký. Celý tento měřící systém je zachycen na Obrázku \ref[fig:lindometer].

\midinsert \clabel[fig:lindometer]{Lindometer}
\picw=8cm \cinspic obr/lindometr.png
\caption/f Měřící systém Lindometer \cite[Chandran20191201]
\endinsert

Výsledky testování Lindometeru v laboratorních podmínkách i na železnici ukázaly   funkčnost systému jak pro detekování chybějících svěrek, tak jeho využitelnost pro korelační měření -- výstupem jsou spolehlivé nezašuměné signály. Autoři práce ale korelaci jako takovou s naměřenými daty neprováděli, tady je nutné dále pokračovat ve výzkumu a analyzovat data i s tímto cílem využití. \cite[Chandran20191201]

\sec Inerciální senzory

Další kategorií senzorů využitelných pro korelační měření rychlosti vlaku jsou inerciální senzory (IMU - Inertial Measurement Unit) \cite[Mei2008]. Tyto senzory obvykle obsahují tři akcelerometry měřící zrychlení ve třech navzájem kolmých osách a tři gyroskopy, které měří úhlovou rychlost kolem daných os.

Dvojici těchto snímačů můžeme umístit na rám podvozku -- každý nad jinou nápravu dvojkolí, která jsou od sebe navzájem posunuta o určitou vzdálenost (= rozvor kol). Každý tento senzor bude měřit pohyby (vibrace) nápravy ve vertikálním směru -- stačí tedy použít jednodušší verzi IMU jednotek měřící pouze v jedné ose. 

Kolejnice nejsou nikdy dokonale hladké, vždy se na nich nějaké nepravidelné nerovnosti vyskytují, které pohybují s dvojkolím nahoru a dolu při přejezdu přes ně. Tyto pohyby se poté přenášejí na podvozek, kde je možné je detekovat. Druhé dvojkolí přes stejnou nerovnost také přejede, ale s drobným časovým zpožděním. Korelací mezi těmito dvěma signály můžeme zjistit dané časové zpoždění a následně při znalosti rozvoru kol dopočítat rychlost pohybu.

Dle výsledků popsaných v \cite[Mei2008] lze touto metodou dosáhnout velmi přesných a spolehlivých výsledků, při měření na vlaku v rychlostech do 200 km/h dosáhli maximální chyby rychlosti menší než 1 \%. 

Nevýhodou této metody je potřeba dostatečně citlivých IMU jednotek, které dokáží spolehlivě tyto malé nerovnosti na kolejích detekovat, které jsou ale už poměrně  cenově nákladné. Další nevýhodou je, že hlavní zdroje těchto nerovností byly dříve kolejnicové spoje (místa, kde se potkávají dva na sebe navazující kusy kolejnic), na kterých docházelo díky spáře a ne vždy identické výšce kolejnic ke známému \uv{drncání} vlaku. V~dnešní době jsou už však tyto spoje nahrazovány bezstykovou kolejí (kolejnice jsou k~sobě svařovány), kde je spoj bez spáry následně dohladka obroušen. I tak se ale na kolejnici vyskytují jiné detekovatelné nerovnosti dané opotřebováním kolejnic, i~když nejsou tak výrazné.

\sec Mikrovlnné senzory

Mikrovlnné senzory, neboli také Dopplerovy radary, jsou senzory rychlosti. Ke své funkci využívají (na rozdíl od předešlých senzorů) Dopplerův jev, tedy změnu frekvence odeslaného a přijatého signálu (vlnění), která je způsobena nenulovou vzájemnou (radiální) rychlostí vysílače a přijímače. Závislost je dána rovnicí

$$
f_r = f_t {c+v \over c-v}, \eqmark
$$

kde $ f_t $ je frekvence odesílaného signálu, $ f_r $ frekvence přijatého signálu, $ c $ rychlost šíření signálu v prostředí (pro elektromagnetické vlnění rychlost světla) a $ v $ je vzájemná rychlost vysílače a přijímače. Z rovnice si již jednoduše vyjádříme hledanou vzájemnou rychlost

$$
\label[eq:mikrovlna]
v = c {f_r-f_t \over f_r+f_t}. \eqmark
$$

Dopplerovy radary obvykle pracují s elektromagnetickým zářením o frekvencích v~řádu desítek GHz, jedná se tedy o mikrovlnné záření. Vyšlou mikrovlnou vlnu o~přesně dané frekvenci, ta se odrazí od měřeného pohybujícího objektu zpět a díky vzájemné rychlosti změní svou frekvenci. Navrácená vlna je přijata zpět v senzoru, kde je analyzována její frekvence a z ní se následně vypočítá vzájemná rychlost. Použitím mikrovlnných vln navíc dochází k eliminaci problému s ušpiněním čela senzoru, mikrovlny usazeným prachem a podobnými malými nečistotami proniknou. Tento způsob dosahuje vysoké přesnosti měření rychlosti.

Měření rychlosti vlaku Dopplerovým radarem je v dnešní době již poměrně běžně používáno a je jednou ze součástí odometrie systému ETCS. Snímač je zde umístěn zespodu lokomotivy a pod určitým úhlem $ \varphi $ snímá povrch kolejového svršku. Radar nemůže snímat kolejový svršek kolmo, protože v tom případě by vzájemná radiální rychlost byla nulová, naopak nemůže být umístěn ani vodorovně (horizontálně), protože by se odeslaná vlna neměla od čeho odrazit zpět. Z tohoto důvodu je nutné upravit Rovnici \ref[eq:mikrovlna], aby reflektovala úhel, pod kterým je povrch snímán \cite[Du2020]

$$
v = c {f_r-f_t \over (f_r+f_t) \cos\varphi}. \eqmark
$$

Schéma funkce senzoru s vyznačeným úhlem $ \varphi $ je na Obrázku \ref[fig:mikrovlny].


\midinsert \clabel[fig:mikrovlny]{Princip funkce Dopplerova radaru}
\picw=10cm \cinspic obr/obr17.png
\caption/f Schéma principu funkce Dopplerova radaru na železnici. Převzato z \cite[Du2020], upraveno.
\endinsert

Pro použití v systému ETCS musí být senzor certifikován, musí dosahovat vysoké spolehlivosti a malé chyby měření. Obvykle jsou používány vícekanálové radary, které měří v jeden moment rychlost na více místech a jednotlivé výsledky analyzují a porovnávají. Tyto požadavky zvyšují cenu senzoru. Výrobcem mikrovlnných radarů pro ETCS odometrii je například firma DEUTA\urlnote{https://www.deuta.com/en/radar-sensors.aspx}.
