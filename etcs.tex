
\label[metody] \chap Současné metody měření rychlosti a polohy vlaku požívané v~praxi

\sec Rychloměry

Zdaleka nejčastějším způsobem měření rychlosti vlaku jsou rychloměry snímající pomocí optické či magnetické značky úhlovou rychlost, s jakou se otáčí kola vlaku (podobně jako fungují rychloměry v automobilu). Ze znalosti poloměru kola vlaku se následně dopočítává aktuální rychlost, případně se integrací získá ujetá vzdálenost.

Jak již bylo naznačeno v úvodu, nejedná se bohužel za některých podmínek o příliš spolehlivý způsob měření rychlosti. Při rozjezdu či zrychlování vlaku může docházet při ztrátě adheze (například díky vlhké/mokré koleji) k nežádoucímu prokluzování kol vlaku, při kterém se kola otáčí rychleji než jaká je skutečná rychlost soupravy. Rychloměr ukazuje díky tomu vyšší rychlost jízdy. 

Opačný problém může nastat při brzdění vlaku, kdy opět vlivem ztráty přilnavosti mezi kolem a kolejnicí se kola vlaku otáčí pomaleji (či vůbec), zatímco souprava se setrvačností pohybuje dále vyšší rychlostí -- vlak v tomto případě klouže neovladatelně po kolejích a rychloměr ukazuje nižší rychlost než ve skutečnosti. Tento jev způsobil v~kombinaci s vysokou rychlostí v~nedávné době některé nehody na české železnici\urlnote{https://www.novinky.cz/krimi/clanek/ze-srazky-vlaku-u-kdyne-vini-strojvedouciho-40368396}.

Strojvedoucí se snaží o maximální ovladatelnost vlaku a oba tyto nežádoucí jevy se pokouší omezit pískováním (posypáním kolejnice před koly jemným pískem), ale v praxi se těmto jevům úplně vyhnout nedá. Rychloměry jsou pro eliminaci falešných rychlostí umisťovány na nápravy, které nejsou poháněny, což ovšem není vždy možné. Především brzdění představuje problém, protože brzdy jsou umístěny na všech nápravách, aby se maximalizoval brzdný účinek. U rychloměrů se také využívá redundance, kdy jsou snímače umístěny na několika nápravách zároveň a jednotlivé měřené hodnoty rychlostí jsou mezi sebou porovnávány a vyhodnocovány. Kvůli bezpečnosti se pak při rozjezdu používá a~zobrazuje minimální měřená rychlost, při zpomalování naopak nejvyšší. \cite[Mei2010]

Integrováním rychlosti v čase dostaneme ujetou vzdálenost vlaku. Pokud ovšem budeme integrovat nepřesné hodnoty rychlosti, tak výsledná vzdálenost bude mít velkou odchylku od skutečně ujeté vzdálenosti, protože chyby měření se při integraci sčítají. V případě, kdy budeme spolehlivě znát ujetou vzdálenost vlaku, můžeme určit polohu kombinací mapových podkladů železniční sítě a pevných bodů, od kterých měříme ujetou vzdálenost (například stanice, přejezdy nebo poslední známá přesná poloha z~GNSS).

\sec Globální družicový navigační systém

GNSS (Global Navigation Satellite System) slouží k určování pozice na Zemi a obsahuje americký poziční systém GPS, ruský GLONASS, evropský systém Galileo, čínský BeiDou a další. Všechny tyto systémy využívají družice na oběžné dráze Země, které vysílají signál s informacemi o své dráze a přesném času směrem k Zemi. Po přijetí signálu z~několika družic je možné pomocí různé doby letu signálu z~různých směrů určit polohu přijímače kdekoliv na Zemi. Jednotlivé systémy samostatně dosahují při širokém otevřeném výhledu na oblohu v civilní variantě přesnosti až jednotek metrů, vzájemnou kombinací systémů je možné tuto přesnost ještě zvýšit. Na základě změny polohy v čase je možné určit i rychlost pohybu přijímače.

Ohledně využití satelitní navigace na železnici v současné chvíli probíhají živé diskuze, nepanuje však zatím shoda na parametrech certifikace, kterých musí jednotlivé lokalizační jednotky dosahovat. Poloha musí být určena ve všech případech s dostatečnou přesností na to, aby bylo možné rozlišit polohu vlaku na jednotlivých kolejích (i ve velkých stanicích). Hlavním bezpečnostním problémem ale stále zůstává relativně velká nespolehlivost v~místech s omezeným výhledem na oblohu, kterých je kolem železnice bohužel hodně -- hustá zástavba ve městech, hlubší údolí, zářezy v kopcích a~skalách či železniční tunely -- zde přesnost systému výrazně klesá a je zde nutné kombinovat GNSS s dalšími senzory určujícími polohu jinými způsoby. Jisté komplikace představuje i~radiofrekvenční rušení od okolních telekomunikačních sítí či rušení přímo od lokomotivy. Potenciál využití GNSS na železnici je obrovský, proto v tomto směru probíhá velký technologický vývoj a po schválení potřebných bezpečnostních parametrů čeká tento systém rychlá implementace do drážních systémů. \cite[Kacmarik2019]

\sec Evropský vlakový zabezpečovací systém

European Train Control System, neboli ETCS, je centrální evropský zabezpečovací systém. Je nedílnou součástí evropského systému řízení železniční dopravy ERTMS (European Rail Traffic Management System). Má postupně nahradit více než dvacet různých národních zabezpečovačů a umožnit cestu jedné soupravy po celé Evropě bez nutnosti výměny lokomotivy na hranici téměř každého státu. Přestože základní parametry ETCS byly definovány již v polovině devadesátých let minulého století, jeho implementace jednotlivými státy a dopravci probíhá velmi pomalu a je velmi finančně nákladná, v České republice zatím tento systém centrálně spuštěn nebyl.

Určování aktuální rychlosti vlaku a ujeté vzdálenosti je nezbytné pro správnou a~bezpečnou funkci celého systému. ETCS vyžaduje vybavení tratě takzvanými Eurobalízami (Obrázek \ref[fig:balise]), které jsou umístěny uprostřed kolejiště v pravidelných intervalech (zpravidla u~hlavních návěstidel). Při přejezdu vlaku se čtečkou nad touto balízou dochází k~přenosu informací mezi vozidlem a zabezpečovacím zařízením, jedna z těchto informací je i přesná poloha této balízy (a tím pádem i vlaku v momentě přejezdu).

Mezi jednotlivými Eurobalízami je také potřebné znát přesnou polohu (respektive ujedou vzdálenost od poslední balízy) a rychlost vlaku, proto v rámci ETCS musí být každá souprava vybavena několika nezávislými odometrickými senzory -- senzory otáček kol (rychloměrem), Dopplerovým radarem (Obrázek \ref[fig:etcs-radar]) či akcelerometry, záložně také GNSS modulem. Senzory pracují nezávisle, aby v případě výpadku jednoho mohlo pokračovat měření dále. Dle požadavků systému nesmí chyba přesnosti integrování měřené rychlosti na ujetou vzdálenost přesáhnout interval $ \pm $(5 m + 5 \%) \cite[nMYCkYXKjfKzFwsC]. Po projetí následující balízy je měřená vzdálenost (stejně tak i aktuální chyba měření) vynulována a měření vzdálenosti začíná znovu od začátku. \cite[2XMPWTfi68W3ulLw]

\midinsert
\line{\hsize=.5\hsize \vtop{%
      \clabel[fig:balise]{Eurobalíza s anténou}
      \picw=7cm \cinspic obr/balise.jpg
      \caption/f Eurobalíza se čtecí anténou na vlaku \cite[Polivka2011]
   \par}\vtop{%
      \clabel[fig:etcs-radar]{Dopplerův radar}
      \picw=7cm \cinspic obr/etcs-radar.jpg
      \caption/f Dopplerův radar na podvozku vlaku \cite[TkA6yIdvRC45vmKT]
   \par}}
\endinsert



