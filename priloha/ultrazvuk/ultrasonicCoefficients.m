format compact;

% pro spusteni je nuté vzdy nechat odkomentovanou jednu cast

A = readmatrix('ultrazvuk1-v0.3.log');      % rychlost pohybu 0.3 m/s
A = A/10;
R1 = correlationCoefficients(A, [374 1103 1793 2437 3065], 124, 226);
R2 = correlationCoefficients(A, [762 1449 2117 2746 3403], 100, 226);


% B = readmatrix('ultrazvuk1-v0.3-2.log');  % rychlost pohybu 0.3 m/s
% B = B/10;
% R1 = correlationCoefficients(B, [338 1310 2242 3193 4047], 124, 226);
% R2 = correlationCoefficients(B, [844 1789 2746 3642 4518], 124, 226);


% C = readmatrix('ultrazvuk1-v1.log');      % rychlost pohybu 1 m/s
% C = C/10;
% R1 = correlationCoefficients(C, [632 1338 2039 2721 3422], 100, 200);
% R2 = correlationCoefficients(C, [970 1688 2378 3081 3813], 100, 200);


% D = readmatrix('ultrazvuk1-v1-2.log');     % rychlost pohybu 1 m/s
% D = D/10;
% R1 = correlationCoefficients(D, [599 1547 2361 3152 3911], 100, 200);
% R2 = correlationCoefficients(D, [1134 1961 2776 3539 4365], 100, 200);

disp('R1 - tam');
max(R1)
min(R1)
mean(R1)

disp('R2 - zpet');
max(R2)
min(R2)
mean(R2)