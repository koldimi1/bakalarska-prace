// inicializace promennych
volatile unsigned long timeStart1 = 0;
volatile unsigned long timeEnd1 = 0;
volatile unsigned long timeStart2 = 0;
volatile unsigned long timeEnd2 = 0;

unsigned long programStart = 0;

// definovani delky periody v mikrosekundach
#define period 10000

void setup() {
  // spolecny triger pin
  pinMode(PB6, OUTPUT);

  // echo piny
  pinMode(PB3, INPUT);
  pinMode(PB4, INPUT);

  // spolecny napajeci pin
  pinMode(PB7, OUTPUT);

  // spusteni seriove komunikace s PC rychlosti 2 Mb/s
  Serial.begin(2000000);

  // zapnuti napajeni senzoru
  digitalWrite(PB7, HIGH);

  // nastaveni interruptu na echo pinech 
  attachInterrupt(digitalPinToInterrupt(PB3), echo1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PB4), echo2, CHANGE);
}

void loop() {
   // zaznamenani casu zacatku cyklu v mikrosekundach
  programStart = micros();

  // nastaveni triger pinu na zem, pote odeslani triger pulzu
  digitalWrite(PB6, LOW);
  delayMicroseconds(2);
  digitalWrite(PB6, HIGH);
  delayMicroseconds(20);
  digitalWrite(PB6, LOW);

  // vynulovani promennych
  timeStart1 = 0;
  timeStart2 = 0;
  timeEnd1 = 0;
  timeEnd2 = 0;


  while (1) {
    
    // cekame dokud neuplyne 9 ms od zacatku periody
    if (micros() - programStart >= period-1000) {

      // vypocet doby trvani jednotlivych echo pulzu
      unsigned int dist1 = (timeEnd1 - timeStart1) * 100 / 58.3;
      unsigned int dist2 = (timeEnd2 - timeStart2) * 100 / 58.3;

      // kontrola chybovych stavu
      if (timeEnd1 == 0 || timeStart1 == 0) {
        dist1 = 0;
      }
      if (timeEnd2 == 0 || timeStart2 == 0) {
        dist2 = 0;
      }

      // odeslani merenych hodbot do PC
      Serial.println((String) "" + dist1 + " " + dist2);

      // resetovani senzoru v pripade chyby
      if (dist1 == 0 ||  dist2 == 0) {
        digitalWrite(PB7, LOW);
        delayMicroseconds(250);
        digitalWrite(PB7, HIGH);
      }

      break;
    }
  }

  // cekani na konec periody
  if (micros()-programStart < period){
      delayMicroseconds((unsigned int)(period-(micros()-programStart)));
  }
}

// funkce spustene trigrem, ulozi aktualni cas
void echo1() {
  if (timeStart1 == 0) {
    timeStart1 = micros();
  } else {
    timeEnd1 = micros();
  }
}

void echo2() {
  if (timeStart2 == 0) {
    timeStart2 = micros();
  } else {
    timeEnd2 = micros();
  }
}
