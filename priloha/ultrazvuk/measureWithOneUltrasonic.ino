// definice funkci pro primy pristup do registru - vyrazne rychlejsi, nez jsou funkce Arduina
// prevzato z http://masteringarduino.blogspot.com/2013/10/fastest-and-smallest-digitalread-and.html

#define portOfPin(P)\
  (((P)>=0&&(P)<8)?&PORTD:(((P)>7&&(P)<14)?&PORTB:&PORTC))
#define ddrOfPin(P)\
  (((P)>=0&&(P)<8)?&DDRD:(((P)>7&&(P)<14)?&DDRB:&DDRC))
#define pinOfPin(P)\
  (((P)>=0&&(P)<8)?&PIND:(((P)>7&&(P)<14)?&PINB:&PINC))
#define pinIndex(P)((uint8_t)(P>13?P-14:P&7))
#define pinMask(P)((uint8_t)(1<<pinIndex(P)))

#define pinAsInput(P) *(ddrOfPin(P))&=~pinMask(P)
#define pinAsOutput(P) *(ddrOfPin(P))|=pinMask(P)
#define digitalLow(P) *(portOfPin(P))&=~pinMask(P)
#define digitalHigh(P) *(portOfPin(P))|=pinMask(P)
#define isHigh(P)((*(pinOfPin(P))& pinMask(P))>0)
#define isLow(P)((*(pinOfPin(P))& pinMask(P))==0)

// definice cisel pinu 
const byte pTrig = 2;
const byte pEcho = 3;
const byte VCC = 4;

// inicializace promennych, do kterych se ukladaji data
unsigned long odezva;
unsigned long vzdalenost;
unsigned long programStart;
bool measuring = true;
volatile unsigned long trigStart = 0;

// nastaveni delky jedne periody mereni v mikrosekundach
const unsigned int duration = 10000;
 
void setup() {
  // nastaveni pinu
  pinAsOutput(pTrig);
  pinAsInput(pEcho);
  pinAsOutput(VCC);
  
  // spusteni seriove komunikace s PC rychlosti 2 Mb/s
  Serial.begin(2000000);

  // zapnuti napajeni senzoru
  digitalHigh(VCC);
}

void loop()
{
   // zaznamenani casu zacatku cyklu v mikrosekundach
   programStart = micros();
   measuring = true;
   
  // nastaveni triger pinu na zem, pote odeslani triger pulzu
  digitalLow(pTrig);
  delayMicroseconds(2);
  digitalHigh(pTrig);
  delayMicroseconds(20);
  digitalLow(pTrig);

  // cekani na zacatek echa
  while(isLow(pEcho)) {
    // ukonceni v pripade, ze se blizi konec periody a echo neprislo
    if (micros()-programStart >= duration-1000){
      measuring = false;
      // odeslani chyboveho stavu
      Serial.println("0");
      break;
    }
  }

  // ulozeni casu nabezne hrany na echo pinu
  trigStart = micros();

  // cekani na konec echa
  while(isHigh(pEcho)) {
    // ukonceni v pripade, ze se blizi konec periody a konec echa neprisel
    if (micros()-programStart >= duration-1000){
      measuring = false;
      // odeslani chyboveho stavu
      Serial.println("1");
      // restart senzoru - vypnuti napajeni
      digitalLow(VCC);
      delayMicroseconds(250);
      digitalHigh(VCC);
      break;
    }
  }

  // pokud echo signal vcas skoncil, prisla senstupna hrana
  if (measuring) {
    // vypocet doby trvani echo pulzu
    odezva = micros()-trigStart;
    // prepocet na vzdalenost
    vzdalenost = odezva*100 / 58.3;
    // odeslani merene vzdalenosti do PC
    Serial.println(vzdalenost);
  }

  // cekani na konec periody
  if (micros()-programStart < duration){
      delayMicroseconds((unsigned int)(duration-(micros()-programStart)));
  } 

}
