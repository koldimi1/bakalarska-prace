

% pro spusteni je nuté vzdy nechat odkomentovanou jednu cast

A = readmatrix('ultrazvuk2-v0.5.log');          % rychlost pohybu 0.5 m/s
% 100,300;300,500;500,700;700,900;900,1100;1100,1300;1300,1500;1500,1700

% A = readmatrix('ultrazvuk2-v0.5-2.log');      % rychlost pohybu 0.5 m/s
% 50,250;250,450;430,630;630,780;780,950;950,1100

% A = readmatrix('ultrazvuk2-v1.log');          % rychlost pohybu 1 m/s
% 80,280;250,450;450,650;650,850;850,1050;1050,1220;1220,1420;1420,1620

% A = readmatrix('ultrazvuk2-v1-2.log');        % rychlost pohybu 1 m/s
% 150,350;350,550;550,750;750,910;910,1110;1110,1310;1310,1480;1480,1660;1660,1830;1830,2030

vzdalenostSenzoru = 0.12;       % vzdalenost senzoru od sebe v m
frekvence = 100;                % frekvence snimani senzoru v Hz

% Nastaveni vychoziho a koncoveho bodu signalu, 
% mezi kterymi se dela korelace.
% Hodnoty techto bodu jsou za sebou pod importem daneho souboru
startPoint = 100;
endPoint = 300;

% nahrazeni mimolezicich bodu prumerem 
TF = isoutlier(A,'quartiles');
T = A;
M = mean(T);
T(TF(:,1),1) = M(1); 
T(TF(:,2),2) = M(2);

% znormovani dat
M2 = mean(T);
B = T-M2;

% vypocet korelace
[c,lags] = xcorr(B(startPoint:endPoint,1),B(startPoint:endPoint,2));

% graf korelacniho koeficientu
figure(3);
stem(lags,c);

% nalezeni posunu mezi signaly
[~, I] = max(c);
shift = lags(I);

% vypocet a vypis rychlosti dle korelace
calcSpeed = -(vzdalenostSenzoru)/(shift/frekvence)
