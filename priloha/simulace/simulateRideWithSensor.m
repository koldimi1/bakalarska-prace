function [vypocitanaRychlost, rozdilRychlosti] = simulateRideWithSensor(model, senzor, speed, vzdalenostSenzoru, actualisation)
%SIMULATERIDE Simulace jizdy se senzory

x = model.x;
y = model.y;
len = model.len;        % delka generovaneho modelu

f = senzor.frekvence;
a = senzor.a;
decimals = senzor.decimals;
std = senzor.std;

speed = speed/3.6;      % prevod rychlosti na m/s
d = speed/f*100;        % vzdalenost dvou vzorku

% vypocet koeficientu okenkove funkce
L = length(0:0.01:2*a);
w = tukeywin(L);
w = w/sum(w);

% inicializace poli pro vysledky mereni senzoru
y1 = zeros(1, actualisation*f);
y2 = zeros(1, actualisation*f);

i = 5;
j = 1;

while len-1-i > 1e-3
    
    % navzorkovani okoli daneho bodu
    window = (i-a):0.01:(i+a);
    ys = interp1(x, y, window);
    % konvoluce a zatizeni sumem
    ynoise = sum(conv(ys, w, 'same')) + randn()*std;
    % zaokrouhleni vysledku na dany pocet desetinnych mist
    y1(1,j) = round(ynoise, decimals);

    % opakovani pro druhy posunuty senzor
    if i-4 >= vzdalenostSenzoru
        window = (i-vzdalenostSenzoru-a):0.01:(i-vzdalenostSenzoru+a);
        ys = interp1(x, y, window);
        ynoise = sum(conv(ys, w, 'same')) + randn()*std;
        y2(1,j) = round(ynoise, decimals);
    end
    
    % posun k dalsimu vzorku
    i = i + d;
    j = j + 1;
    if j > actualisation*f
        break;
    end
end

% vypocet korelace
[c,lags] = xcorr(y1,y2);

% nalezeni posunu mezi signaly
[~, I] = max(c);
shift = lags(I);

% vypocet rychlosti a rozdilu rychlosti v km/h
vypocitanaRychlost = (-(vzdalenostSenzoru/100)/(shift/f))*3.6;
rozdilRychlosti = (speed*3.6-vypocitanaRychlost);

end
