 function world = generateWorld(vzdalenostPrazce, velikostPrazce, delkaModelu)
% GENERATEWORLD Vytvoreni modelu kolejoveho svrsku

x = 0:1:delkaModelu-1;
% vygenerovani nahodnych hodnot
y = randn(1, delkaModelu);

i = 0;
while i <= delkaModelu
    % vlozeni prazce
    if mod(i, vzdalenostPrazce) == 0
        y(i+1:i+velikostPrazce + 2) = 0;
        i = i + velikostPrazce + 3;
        continue;
        
    % vyhlazeni hodnot
    elseif (mod(i, 3) == 0 && i ~= delkaModelu) || (mod(i, 3) == 1 && i ~= delkaModelu)
        y(i) = interp1([1 3], [y(i-1) y(i+1)], 2);
    end
    i = i + 1;
end


% vyhlazeni dat
y = smoothdata(y);
y = smoothdata(y);

world.x = x;
world.y = y;
world.len = delkaModelu;

 end
