format long;
format compact;
clear;

% parametry modelu kolejoveho svrsku
velikostPrazce = 25;            % velikost prazce v cm
vzdalenostPrazce = 60;          % vzdalenost 2 prazcu v cm
delkaModelu = 10000;            % delka generovaneho modelu v cm

% parametry simulace jizdy
vzdalenostSenzoru = 200;        % vzdalenost mezi 2 senzory v cm
vyskaSenzoru = 40;              % vyska senzoru nad povrchem
periodaAktualizace = 1;         % perioda aktualizace v s

speeds = 8:2:160;               % rychlosti pohybu v km/h

% vygenerovani profilu kolejoveho svrsku
model = generateWorld(vzdalenostPrazce, velikostPrazce, delkaModelu);


% Parametry pouzitych senzoru

% TF-Luna
sensors(1).name = 'TF-Luna';    % nazev senzoru
sensors(1).frekvence = 250;     % snimaci frekvence senzoru
sensors(1).FoV = 2;             % zorny uhel - field of view
sensors(1).a = tan(deg2rad(sensors(1).FoV/2))*vyskaSenzoru;     % velikost opticke stopy
sensors(1).decimals = 0;        % pocet desetinnych mist
sensors(1).std = 0.25;          % smerodatna odchylka
sensors(1).sensorNumber = 1;    % poradi senzoru

% TF Mini-S
sensors(2).name = 'TF Mini-S';
sensors(2).frekvence = 1000;
sensors(2).FoV = 2;
sensors(2).a = tan(deg2rad(sensors(2).FoV/2))*vyskaSenzoru;
sensors(2).decimals = 0;
sensors(2).std = 0.25;
sensors(2).sensorNumber = 2;

% TF03-100
sensors(3).name = 'TF03-100';
sensors(3).frekvence = 10000;
sensors(3).FoV = 0.5;
sensors(3).a = tan(deg2rad(sensors(3).FoV/2))*vyskaSenzoru;
sensors(3).decimals = 0;
sensors(3).std = 2;
sensors(3).sensorNumber = 3;

% Baumer - ILD1420-500
sensors(4).name = 'OM70-P0600';
sensors(4).frekvence = 2000;
sensors(4).a = 0.03/2;
sensors(4).decimals = 3;
sensors(4).std = 0.0009;
sensors(4).sensorNumber = 4;

% ILD1420-500
sensors(5).name = 'ILD1420-500';
sensors(5).frekvence = 4000;
sensors(5).a = 0.11/2;
sensors(5).decimals = 2;
sensors(5).std = 0.004;
sensors(5).sensorNumber = 5;


vypocitaneRychlosti = zeros(size(sensors, 2), size(speeds, 2));
odchylkyRychlosti = zeros(size(sensors, 2), size(speeds, 2));

% simulace samotne jizdy kazdeho senzoru pro kazdou rychlost
for sensor=sensors
    disp(sensor.name);
    index = 1;
    for speed=speeds
        disp(speed);
        [vypocitanaRychlost, rozdilRychlosti] = simulateRideWithSensor(model, sensor, speed, vzdalenostSenzoru, periodaAktualizace);
        vypocitaneRychlosti(sensor.sensorNumber, index) = vypocitanaRychlost;
        odchylkyRychlosti(sensor.sensorNumber, index) = rozdilRychlosti;
        index = index + 1;
    end
end

% vykresleni vysledku
figure(1);
plot(speeds, odchylkyRychlosti, 'LineWidth',2);
grid on;
set(gca,'FontSize',15)
title(legend('TF-Luna - 0.25 kHz', 'TF Mini-S - 1 kHz', 'TF03-100 - 10 kHz', 'OM70-P0600 - 2 kHz', 'ILD1420-500 - 4 kHz','Location','northwest', 'FontSize',14), 'Senzory');
title('Odchylka od skutečné rychlosti pro použité senzory');
xlabel('Rychlost [km/h]', 'FontWeight','bold');
ylabel('Odchylka [km/h]', 'FontWeight','bold');

