format compact;

% pro spusteni je nuté vzdy nechat odkomentovanou jednu cast


% A = readmatrix('optika-kameny-v0.5.log');      % rychlost pohybu 0.5 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [2529 7153 11968 16743 21382], 1000, 1000);
% R2 = correlationCoefficients(A, [4350 9165 13990 18692 23251], 1000, 1000);

% A = readmatrix('optika-kameny-v0.5-2.log');   % rychlost pohybu 0.5 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [1991 7186 12267 16970 21846], 1000, 1000);
% R2 = correlationCoefficients(A, [4143 9115 14147 18929 23734], 1000, 1000);

% A = readmatrix('optika-kameny-v1.log');       % rychlost pohybu 1 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [3692 8082 12454 16471 20769], 1000, 1000);
% R2 = correlationCoefficients(A, [6314 10572 14875 18849 23436], 1000, 1000);

% A = readmatrix('optika-kameny-v1-2.log');     % rychlost pohybu 1 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [2800 6876 10755 15533 20046], 1000, 1000);
% R2 = correlationCoefficients(A, [5193 9026 13612 18303 23343], 1000, 1000);


disp('R1 - tam');
max(R1)
min(R1)
mean(R1)

disp('R2 - zpet');
max(R2)
min(R2)
mean(R2)