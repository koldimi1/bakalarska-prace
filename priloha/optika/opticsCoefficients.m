format compact;

% pro spusteni je nuté vzdy nechat odkomentovanou jednu cast

A = readmatrix('optika-v0.3.log');      % rychlost pohybu 0.3 m/s
A = rmmissing(A);
R1 = correlationCoefficients(A, [2492 5839 9106 12478 15593], 500, 500);
R2 = correlationCoefficients(A, [4223 7515 10823 14119 17251], 500, 500);

% A = readmatrix('optika-v0.3-2.log');  % rychlost pohybu 0.3 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [2558 5904 8996 11941 14858], 500, 500);
% R2 = correlationCoefficients(A, [4257 7611 10586 13504 16394], 500, 500);

% A = readmatrix('optika-v1.log');      % rychlost pohybu 1 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [2842 5649 8451 11161 13882], 400, 400);
% R2 = correlationCoefficients(A, [4262 7115 9818 12597 15374], 500, 500);

% A = readmatrix('optika-v1-2.log');    % rychlost pohybu 1 m/s
% A = rmmissing(A);
% R1 = correlationCoefficients(A, [2568 5267 8160 11049 13829], 400, 400);
% R2 = correlationCoefficients(A, [3972 6753 9558 12512 15271], 400, 400);


disp('R1 - tam');
max(R1)
min(R1)
mean(R1)

disp('R2 - zpet');
max(R2)
min(R2)
mean(R2)