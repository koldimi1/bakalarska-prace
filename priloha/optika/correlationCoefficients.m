function R = correlationCoefficients(A, peaks, startShift, endShift)
%correlationCoefficients Vypocet korelacnich koeficientu mezi signaly

n = size(peaks,2);
X = zeros(startShift+endShift+1, n);
R = zeros(1, (n*(n-1)/2));

% normalizace dat
for i = 1:n
    X(:,i) = A(peaks(i) - startShift:peaks(i) + endShift) - mean(A(peaks(i) - startShift:peaks(i) + endShift));
end

% overeni, ze posun mezi signaly je 0
for i = 1:(n-1)
    [c,lags] = xcorr(X(:,i),X(:,i+1));
    [~, I] = max(c);
    shift = lags(I);
    if shift ~= 0 
        disp("Peak point is not correct!");
        disp(i);
        disp(shift);
    end
end

% vypocet a ulozeni korelacnich koeficientu
counter = 1;
for i = 1:n
    for j = (i+1):n
        coeff = corrcoef(X(:,i),X(:,j));
        R(1, counter) = coeff(1,2);
        counter = counter + 1;
    end
end

end

